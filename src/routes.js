import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Constants from "expo-constants";

import { View, Text, StyleSheet, Platform, StatusBar } from "react-native";
import { useAuth } from "./contexts/auth";

import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import Entradas from "./pages/Entradas";
import Saidas from "./pages/Saidas";
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";
import Form from "./components/Form";

const Drawer = createDrawerNavigator();
const AppStack = createStackNavigator();

const styles = StyleSheet.create({
	loadingBox: {
		backgroundColor: "#7cbed6",
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	loadingText: {
		color: "#FFF",
		fontFamily: "Roboto_500Medium",
		fontSize: 32,
	},
});

const MyDrawer = () => {
	return (
		<Drawer.Navigator
			drawerPosition="right"
			screenOptions={{ swipeEnabled: false }}
		>
			<Drawer.Screen name="Dashboard" component={Dashboard} />
			<Drawer.Screen name="Entradas" component={Entradas} />
			<Drawer.Screen name="Saidas" component={Saidas} />
		</Drawer.Navigator>
	);
};

const Routes = () => {
	const { signed, loading } = useAuth();

	if (loading) {
		return (
			<View style={styles.loadingBox}>
				<Text style={styles.loadingText}>Loading...</Text>
			</View>
		);
	}

	return (
		<SafeAreaProvider>
			<SafeAreaView
				style={{
					flex: 1,
					backgroundColor: "#7cbed6",
				}}
			>
				<NavigationContainer>
					<AppStack.Navigator
						screenOptions={{
							headerShown: false,
						}}
					>
						{signed ? (
							<>
								<AppStack.Screen name="MyDrawer" component={MyDrawer} />
								<AppStack.Screen name="formEntrada" component={Form} />
								<AppStack.Screen name="formSaida" component={Form} />
							</>
						) : (
							<AppStack.Screen name="Login" component={Login} />
						)}
					</AppStack.Navigator>
				</NavigationContainer>
			</SafeAreaView>
		</SafeAreaProvider>
	);
};

export default Routes;
