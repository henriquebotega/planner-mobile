import React, { createContext, useContext, useState, useEffect } from "react";
import {
	signIn,
	signOutGoogle,
	getBills,
	getBillByID,
	saveBill,
} from "../services/auth";
import AsyncStorage from "@react-native-community/async-storage";
import { decode as atob, encode as btoa } from "base-64";
import * as Google from "expo-google-app-auth";
import api from "../services/api";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
	const [user, setUser] = useState(null);
	const [bill, setBill] = useState(null);
	const [bills, setBills] = useState([]);
	const [tmpItems, setTmpItems] = useState([]);
	const [loading, setLoading] = useState(true);
	const [loadingBill, setLoadingBill] = useState(true);

	const config = {
		iosClientId:
			"562018176403-jq2c48l2nmf3f0d5ut4iedamn8li6u64.apps.googleusercontent.com",
		androidClientId:
			"562018176403-0g6qgshmcahi4pbq5i8idhap3ghd68vd.apps.googleusercontent.com",
		scopes: ["profile", "email"],
	};

	async function getLocalStorage() {
		try {
			const storagedUser = await AsyncStorage.getItem("authrn-user");
			const storagedToken = await AsyncStorage.getItem("authrn-token");

			if (storagedUser && storagedToken) {
				setUser(JSON.parse(storagedUser));
			}
		} catch (e) {}
	}

	useEffect(() => {
		getLocalStorage();
		setLoading(false);
	}, []);

	const [dados, setDados] = useState([
		Math.random() * 100,
		Math.random() * 100,
		Math.random() * 100,
		Math.random() * 100,
		Math.random() * 100,
		Math.random() * 100,
	]);

	const reloadGrafico = () => {
		setDados([
			Math.random() * 100,
			Math.random() * 100,
			Math.random() * 100,
			Math.random() * 100,
			Math.random() * 100,
			Math.random() * 100,
		]);
	};

	const logInAsync = async () => {
		setLoading(true);

		try {
			const resGoogle = await Google.logInAsync(config);

			if (resGoogle.type && resGoogle.type !== "cancel") {
				const profile = {
					googleId: resGoogle.user.id,
					name: resGoogle.user.name,
					email: resGoogle.user.email,
					access_token: resGoogle.accessToken,
				};

				const res = await api.post("/google/account", { profile });

				const { email, password } = res.data;
				login(email, password);
			}
		} catch (e) {
			setLoading(false);
		} finally {
			setLoading(false);
		}
	};

	const login = async (email, password) => {
		setLoading(true);
		const { data } = await signIn(email, password);

		if (data.error) {
			setLoading(false);
			return;
		}

		const { token } = data;

		var base64 = token.split(".")[1];
		const jwtData = JSON.parse(atob(base64));

		const user = jwtData.item;
		setUser(user);

		try {
			await AsyncStorage.setItem("authrn-user", JSON.stringify(user));
			await AsyncStorage.setItem("authrn-token", token);
		} catch (e) {}

		setLoading(false);
	};

	const logout = async () => {
		setLoading(true);
		const { access_token } = user;

		if (access_token) {
			try {
				await signOutGoogle(access_token);
			} catch (e) {}
		}

		await AsyncStorage.clear();
		setUser(null);
		setLoading(false);
	};

	const delEntrada = async (id) => {
		await api.delete(`/bills/${id}`);

		const newBills = bills.filter((i) => i._id != id);
		setBills(newBills);
	};

	const getAllBills = async (tipo) => {
		const { data } = await getBills();
		let res = data || [];

		if (tipo == "ENTRADA") {
			res = res.filter((f) => f.type == "ENTRADA");
		}

		if (tipo == "SAIDA") {
			res = res.filter((f) => f.type == "SAIDA");
		}

		setBills(res);
		setLoadingBill(false);
	};

	const saveNewBill = async (item) => {
		const { data } = await saveBill(item);

		setBills([...bills, data]);
		setLoadingBill(false);
	};

	const getBill = async (id) => {
		const { data } = await getBillByID(id);
		setBill(data);
		setLoadingBill(false);
	};

	return (
		<AuthContext.Provider
			value={{
				signed: !!user,
				user,
				loading,

				dados,
				reloadGrafico,
				bills,
				getAllBills,
				delEntrada,

				getBill,
				bill,
				setBill,
				loadingBill,
				saveNewBill,
				setLoadingBill,

				tmpItems,
				setTmpItems,

				setLoading,
				logInAsync,
				login,
				logout,
			}}
		>
			{children}
		</AuthContext.Provider>
	);
};

export function useAuth() {
	const context = useContext(AuthContext);
	return context;
}
