import api from "./api";
import * as Google from "expo-google-app-auth";

const config = {
	iosClientId:
		"562018176403-jq2c48l2nmf3f0d5ut4iedamn8li6u64.apps.googleusercontent.com",
	androidClientId:
		"562018176403-0g6qgshmcahi4pbq5i8idhap3ghd68vd.apps.googleusercontent.com",
	scopes: ["profile", "email"],
};

export const signIn = async (email, password) => {
	try {
		return await api.post("/login", { email, password });
	} catch (error) {
		return error;
	}
};

export const signOutGoogle = async (access_token) => {
	try {
		return await Google.logOutAsync({ accessToken: access_token, ...config });
	} catch (error) {
		return error;
	}
};

export const getBills = async () => {
	try {
		return await api.get("/bills");
	} catch (error) {
		return error;
	}
};

export const getBillByID = async (id) => {
	try {
		return await api.get("/bills/" + id);
	} catch (error) {
		return error;
	}
};

export const saveBill = async (item) => {
	try {
		return await api.post("/bills", item);
	} catch (error) {
		return error;
	}
};
