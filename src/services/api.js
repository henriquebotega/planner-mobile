import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

const api = axios.create({
	baseURL: "https://planner-backend-token.herokuapp.com/api",
});

api.interceptors.request.use(
	async function (config) {
		config.headers["x-access-token"] = await AsyncStorage.getItem(
			"authrn-token"
		);
		return config;
	},
	function (error) {
		return Promise.reject(error);
	}
);

api.interceptors.response.use(
	function (response) {
		return response;
	},
	async function (error) {
		if (error.response && error.response.status === 401) {
			await AsyncStorage.clear();
			// window.location.href = "/login";
		}

		if (!error.response.data.auth) {
			return error.response;
		}

		return Promise.reject(error);
	}
);

export default api;
