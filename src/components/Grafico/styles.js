import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	buttonIcon: {
		height: 60,
		width: 60,
		backgroundColor: "rgba(0, 0, 0, 0.1)",
		justifyContent: "center",
		alignItems: "center",
	},
});

export default styles;
