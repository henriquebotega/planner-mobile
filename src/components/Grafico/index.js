import React, { useState } from "react";
import { LineChart } from "react-native-chart-kit";
import { View, Dimensions, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";
import { useAuth } from "../../contexts/auth";

import styles from "./styles";

const Grafico = () => {
	const { dados, reloadGrafico } = useAuth();

	return (
		<>
			<View>
				<LineChart
					data={{
						labels: ["January", "February", "March", "April", "May", "June"],
						datasets: [
							{
								data: dados,
							},
						],
					}}
					width={Dimensions.get("window").width - 16} // from react-native
					height={220}
					chartConfig={{
						backgroundColor: "#e26a00",
						backgroundGradientFrom: "#fb8c00",
						backgroundGradientTo: "#ffa726",
						decimalPlaces: 2, // optional, defaults to 2dp
						color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
						style: {
							borderRadius: 16,
						},
					}}
					bezier
					style={{
						marginVertical: 8,
						borderRadius: 16,
					}}
				/>

				<TouchableOpacity onPress={() => reloadGrafico()}>
					<View style={styles.buttonIcon}>
						<Text>
							<AntDesign size={24} color="#fff" name="reload1" />
						</Text>
					</View>
				</TouchableOpacity>
			</View>
		</>
	);
};

export default Grafico;
