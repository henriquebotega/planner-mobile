import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	container: {
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "space-between",
	},
	// centeredView: {
	// 	justifyContent: "center",
	// 	alignItems: "center",
	// },
	// modalView: {
	// 	borderWidth: 1,
	// 	flex: 1,
	// 	backgroundColor: "white",
	// 	borderRadius: 5,
	// 	padding: 35,
	// 	alignItems: "center",
	// 	shadowColor: "#000",
	// 	shadowOffset: {
	// 		width: 0,
	// 		height: 2,
	// 	},
	// 	shadowOpacity: 0.25,
	// 	shadowRadius: 3.84,
	// 	elevation: 5,
	// },
});

export default styles;
