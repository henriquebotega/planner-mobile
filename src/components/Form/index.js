import React, { useState, useEffect } from "react";
import { View, Text, Button, TextInput, Switch, Modal } from "react-native";
import ItemForm from "../ItemForm";

import { useNavigation } from "@react-navigation/native";
import { useAuth } from "../../contexts/auth";

import styles from "./styles";

const Form = ({ route }) => {
	const {
		bill,
		setBill,
		getBill,
		loadingBill,
		saveNewBill,
		setLoadingBill,
		tmpItems,
		setTmpItems,
	} = useAuth();

	const [tipo, setTipo] = useState(route.params.type);
	const [isMultiple, setIsMultiple] = useState(false);
	const [title, setTitle] = useState("");
	const [value, setValue] = useState("0.00");
	const [user_id, setUserId] = useState(null);

	const navigation = useNavigation();

	const toggleSwitch = () => {
		setIsMultiple((previousState) => !previousState);

		if (!isMultiple) {
			setTmpItems([]);
		}
	};

	const preencheForm = () => {
		setTitle(bill.title);
		setIsMultiple(bill.multiple);
		setValue(bill.value.toString());
	};

	useEffect(() => {
		if (bill) {
			preencheForm();
			setUserId(bill.user_id?._id);
		}
	}, [bill]);

	useEffect(() => {
		setBill(null);
		setTmpItems([]);
		setLoadingBill(true);

		const { id } = route.params;

		if (id) {
			getBill(id);
		} else {
			setLoadingBill(false);
		}
	}, []);

	const voltar = () => {
		setBill(null);
		setTmpItems([]);
		navigation.navigate("Entradas");
	};

	const salvar = async () => {
		await saveNewBill({
			type: tipo,
			multiple: isMultiple,
			title,
			value,
			user_id,
		});

		navigation.navigate("Entradas");
	};

	const plusBill = () => {
		const newItem = {
			id: Math.floor(Math.random() * 9999),
			title: "",
			value: "0.00",
			isNew: true,
		};

		setTmpItems([...tmpItems, newItem]);
	};

	const editBill = (item) => {};

	const delBill = (item) => {
		const currentItems = tmpItems.filter((f) => f.id != item.id);
		setTmpItems(currentItems);
	};

	if (loadingBill) {
		return <Text>Aguarde...</Text>;
	}

	return (
		<View style={styles.container}>
			<Button onPress={() => voltar()} title="Voltar" />

			<View
				style={{
					flexDirection: "column",
				}}
			>
				<View
					style={{
						flexDirection: "row",
						justifyContent: "space-between",
					}}
				>
					<View style={{ flexDirection: "column" }}>
						<Text>Multiple</Text>
						<Switch
							trackColor={{ false: "#767577", true: "#81b0ff" }}
							thumbColor={isMultiple ? "#f5dd4b" : "#f4f3f4"}
							ios_backgroundColor="#3e3e3e"
							onValueChange={toggleSwitch}
							value={isMultiple}
						/>
					</View>

					<View style={{ flexDirection: "column" }}>
						<Text>Title</Text>
						<TextInput
							style={{ width: 100, borderColor: "gray", borderWidth: 1 }}
							onChangeText={(text) => setTitle(text)}
							value={title}
						/>
					</View>

					<View style={{ flexDirection: "column" }}>
						<Text>Valor Previsto</Text>
						<TextInput
							style={{ width: 100, borderColor: "gray", borderWidth: 1 }}
							onChangeText={(text) => setValue(text)}
							value={value}
						/>
					</View>

					<View style={{ flexDirection: "column" }}>
						<Text>Valor Gasto</Text>
						<TextInput
							editable={!isMultiple}
							style={{ width: 100, borderColor: "gray", borderWidth: 1 }}
						/>
					</View>

					{isMultiple && <Button onPress={() => plusBill()} title="+" />}
				</View>

				{isMultiple &&
					tmpItems.map((obj, i) => {
						return (
							<ItemForm
								key={i}
								item={obj}
								delBill={delBill}
								editBill={editBill}
							/>
						);
					})}

				{/* <Text>{JSON.stringify(bill)}</Text> */}
			</View>

			<Button onPress={() => salvar()} title="Salvar" />
		</View>
	);
};
export default Form;
