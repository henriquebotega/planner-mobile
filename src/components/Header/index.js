import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Entypo, AntDesign } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { useAuth } from "../../contexts/auth";

import styles from "./styles";

const Header = ({ title }) => {
	const { reloadGrafico } = useAuth();

	const navigation = useNavigation();
	const [selectedPeriod, setSelectedPeriod] = useState();

	const [periods, setPeriods] = useState([
		{ id: 0, title: "Dec/19", year: 2019, month: 12 },
		{ id: 1, title: "Jan/20", year: 2020, month: 1 },
		{ id: 2, title: "Fev/20", year: 2020, month: 2 },
	]);

	useEffect(() => {
		setSelectedPeriod(periods[periods.length - 1]);
	}, []);

	const previousMonth = () => {
		if (selectedPeriod.id > 0) {
			setSelectedPeriod(periods[selectedPeriod.id - 1]);
			reloadGrafico();
		}
	};

	const nextMonth = () => {
		if (selectedPeriod.id < periods[periods.length - 1].id) {
			setSelectedPeriod(periods[selectedPeriod.id + 1]);
			reloadGrafico();
		}
	};

	return (
		<View style={styles.header}>
			<View style={styles.col}>
				<Text style={styles.headerTitle}>{title}</Text>
			</View>

			<View style={[styles.col, styles.selectPeriod]}>
				<TouchableOpacity onPress={() => previousMonth()}>
					<AntDesign name="caretleft" size={24} color="black" />
				</TouchableOpacity>

				<Text style={styles.headerTitle}>{selectedPeriod?.title}</Text>

				<TouchableOpacity onPress={() => nextMonth()}>
					<AntDesign name="caretright" size={24} color="black" />
				</TouchableOpacity>
			</View>

			<View style={[styles.col, styles.menu]}>
				<TouchableOpacity onPress={() => navigation.openDrawer()}>
					<Entypo name="menu" size={32} color="black" />
				</TouchableOpacity>
			</View>
		</View>
	);
};
export default Header;
