import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	header: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
	},
	headerTitle: {
		color: "#fff",
	},
	col: {
		flex: 1,
	},
	selectPeriod: {
		flex: 2,
		justifyContent: "space-around",
		flexDirection: "row",
		alignItems: "center",
	},
	menu: {
		alignItems: "flex-end",
	},
});

export default styles;
