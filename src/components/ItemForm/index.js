import React, { useState } from "react";
import { Platform, View, Text, Button, TextInput } from "react-native";

import DateTimePicker from "@react-native-community/datetimepicker";
import { Container } from "./styles";

function ItemForm({ item, delBill, editBill }) {
	const [isEdit, setIsEdit] = useState(item.isNew || false);
	const [show, setShow] = useState(false);

	const [date, setDate] = useState(new Date(1598051730000));

	const [value, onChangeText] = React.useState("0.00");
	const showDatepicker = () => {
		setShow(true);
	};

	const onChange = (event, selectedDate) => {
		const currentDate = selectedDate || date;
		setShow(Platform.OS === "ios");
		setDate(currentDate);
	};

	return (
		<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
			<View style={{ flexDirection: "column" }}>
				<Text>ItemsBill</Text>
				<Text>#{item.id}</Text>
			</View>

			<View style={{ flexDirection: "column" }}>
				<Text>Date</Text>

				{!isEdit && <Text>{JSON.stringify(date)}</Text>}

				{isEdit && (
					<>
						<Button onPress={showDatepicker} title="Show date picker!" />

						{show && (
							<DateTimePicker
								value={date}
								mode={"date"}
								display="default"
								onChange={onChange}
							/>
						)}
					</>
				)}
			</View>

			<View style={{ flexDirection: "column" }}>
				<Text>Title</Text>

				{!isEdit && <Text>{item.title}</Text>}

				{isEdit && (
					<TextInput
						style={{ width: 100, borderColor: "gray", borderWidth: 1 }}
						onChangeText={(text) => onChangeText(text)}
						value={item.title}
					/>
				)}
			</View>

			<View style={{ flexDirection: "column" }}>
				<Text>Value</Text>
				{!isEdit && <Text>{item.value}</Text>}

				{isEdit && (
					<TextInput
						style={{ width: 100, borderColor: "gray", borderWidth: 1 }}
						onChangeText={(text) => onChangeText(text)}
						value={item.value}
					/>
				)}
			</View>

			{!isEdit && <Button onPress={() => setIsEdit(true)} title="edit" />}
			{isEdit && (
				<Button
					onPress={() => {
						editBill(item);
						setIsEdit(false);
					}}
					title="save"
				/>
			)}

			{!isEdit && <Button onPress={() => delBill(item)} title="del" />}
			{isEdit && <Button onPress={() => setIsEdit(false)} title="cancel" />}
		</View>
	);
}

export default ItemForm;
