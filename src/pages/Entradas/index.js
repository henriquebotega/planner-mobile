import React, { useEffect } from "react";
import { View, Text, FlatList, Button } from "react-native";
import Header from "../../components/Header";
import { useAuth } from "../../contexts/auth";
import { useNavigation } from "@react-navigation/native";

import styles from "./styles";

const Entradas = () => {
	const navigation = useNavigation();
	const {
		setBill,
		loadingBill,
		setLoadingBill,
		bills,
		getAllBills,
		delEntrada,
		setTmpItems,
	} = useAuth();

	useEffect(() => {
		setBill(null);
		setTmpItems([]);
		setLoadingBill(true);

		getAllBills("ENTRADA");
	}, []);

	const incluir = () => {
		navigation.navigate("formEntrada", { type: "ENTRADA" });
	};

	const editar = (id) => {
		navigation.navigate("formEntrada", { type: "ENTRADA", id });
	};

	const excluir = (id) => {
		delEntrada(id);
	};

	if (loadingBill) {
		return <Text>Aguarde...</Text>;
	}

	return (
		<View style={styles.container}>
			<Header title="Entradas" />

			<Button onPress={() => incluir()} title="Novo" />
			{/* <Text>{JSON.stringify(bills)}</Text> */}

			<FlatList
				style={{ flex: 1 }}
				data={bills}
				renderItem={({ item }) => (
					<View style={styles.grid}>
						<Text>{item.title}</Text>
						<Text>{item.value}</Text>
						<Button onPress={() => editar(item._id)} title="Editar" />
						<Button onPress={() => excluir(item._id)} title="Excluir" />
					</View>
				)}
				keyExtractor={(item) => item._id}
			/>
		</View>
	);
};

export default Entradas;
