import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	container: {
		backgroundColor: "#7cbed6",
		flex: 1,
		padding: 8,
	},
	main: {
		flex: 1,
	},
	boxBtn: {
		alignItems: "center",
	},
	button: {
		backgroundColor: "#3366FF",
		width: 300,
		height: 60,
		flexDirection: "row",
		borderRadius: 10,
		alignItems: "center",
	},
	buttonIcon: {
		height: 60,
		width: 60,
		backgroundColor: "rgba(0, 0, 0, 0.1)",
		justifyContent: "center",
		alignItems: "center",
	},
	buttonText: {
		flex: 1,
		textAlign: "center",
		color: "#FFF",
		fontFamily: "Roboto_500Medium",
		fontSize: 16,
	},
});

export default styles;
