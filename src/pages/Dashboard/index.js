import React, { useEffect } from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import { useAuth } from "../../contexts/auth";
import { SimpleLineIcons } from "@expo/vector-icons";

import styles from "./styles";
import Grafico from "../../components/Grafico";
import Header from "../../components/Header";

const Dashboard = () => {
	const { logout, user, bills, getAllBills } = useAuth();

	useEffect(() => {
		getAllBills();
	}, []);

	return (
		<View style={styles.container}>
			<Header title="Dashboard" />

			<View style={styles.main}>
				<Grafico />

				{/* <Text>{JSON.stringify(user)}</Text> */}

				{bills.length > 0 && (
					<FlatList
						data={bills}
						renderItem={({ item }) => (
							<View>
								<Text>{item.title}</Text>
							</View>
						)}
						keyExtractor={(item) => String(item._id)}
					/>
				)}

				<View style={styles.boxBtn}>
					<TouchableOpacity style={styles.button} onPress={() => logout()}>
						<View style={styles.buttonIcon}>
							<Text>
								<SimpleLineIcons size={24} color="#fff" name="logout" />
							</Text>
						</View>

						<Text style={styles.buttonText}>Sair do sistema</Text>
					</TouchableOpacity>
				</View>
			</View>
		</View>
	);
};

export default Dashboard;
