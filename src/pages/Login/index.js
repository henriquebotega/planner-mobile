import React, { useState } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { useAuth } from "../../contexts/auth";
import styles from "./styles";
import { AntDesign } from "@expo/vector-icons";

const Login = () => {
	const { user, loading, logInAsync } = useAuth();

	return (
		<View style={styles.container}>
			<View style={styles.main}>
				<Image
					source={require("../../assets/fundo.jpg")}
					style={styles.imagemFundo}
				/>
				<Text style={styles.title}>Planner</Text>

				<TouchableOpacity style={styles.button} onPress={() => logInAsync()}>
					<View style={styles.buttonIcon}>
						<Text>
							<AntDesign size={24} color="#fff" name="google" />
						</Text>
					</View>

					<Text style={styles.buttonText}>Google Account</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

export default Login;
