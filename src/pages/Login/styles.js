import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	container: {
		backgroundColor: "#7cbed6",
		flex: 1,
		padding: 32,
	},
	main: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	imagemFundo: {
		width: "50%",
		height: 200,
		borderRadius: 10,
		resizeMode: "contain",
	},
	title: {
		color: "#322153",
		fontSize: 32,
		fontFamily: "Roboto_500Medium",
	},
	button: {
		backgroundColor: "#3366FF",
		height: 60,
		flexDirection: "row",
		borderRadius: 10,
		overflow: "hidden",
		alignItems: "center",
	},
	buttonIcon: {
		height: 60,
		width: 60,
		backgroundColor: "rgba(0, 0, 0, 0.1)",
		justifyContent: "center",
		alignItems: "center",
	},
	buttonText: {
		flex: 1,
		justifyContent: "center",
		textAlign: "center",
		color: "#FFF",
		fontFamily: "Roboto_500Medium",
		fontSize: 16,
	},
});

export default styles;
