import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	container: {
		backgroundColor: "#7cbed6",
		flex: 1,
		padding: 8,
	},
});

export default styles;
