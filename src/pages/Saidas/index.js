import React from "react";
import { View, Text } from "react-native";
import Header from "../../components/Header";

import styles from "./styles";

const Saidas = () => {
	return (
		<View style={styles.container}>
			<Header title="Saida" />
			<Text>Saida</Text>
		</View>
	);
};

export default Saidas;
