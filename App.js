import React from "react";
import Routes from "./src/routes";
import { StatusBar } from "react-native";

import { AppLoading } from "expo";

import {
	Roboto_400Regular,
	Roboto_500Medium,
	useFonts,
} from "@expo-google-fonts/roboto";

import { AuthProvider } from "./src/contexts/auth";

export default function App() {
	const [fontsLoaded] = useFonts({
		Roboto_400Regular,
		Roboto_500Medium,
	});

	if (!fontsLoaded) {
		return <AppLoading />;
	}

	return (
		<>
			<StatusBar
				barStyle="dark-content"
				backgroundColor="transparent"
				translucent
			/>

			<AuthProvider>
				<Routes />
			</AuthProvider>
		</>
	);
}
